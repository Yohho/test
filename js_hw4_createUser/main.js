////// Incorrect way, but still work! ////////////////////////////////////////////////////////////////////////
//
// let askName = prompt("What is ur name");
// let askLastName = prompt("What is your last name");
//
// function createNewUser(name, lastName) {
//     let newUser = {
//         firstName: name,
//         lastName: lastName,
//         login: function (a, b) {
//             return name[0].toLowerCase() + lastName.toLowerCase()
//         }
//     };
//     return newUser.login();
// }
//
//
// console.log( createNewUser(askName, askLastName));

///////////////////////////////////////////////////////////////////////////////////////////////////////////////

////// Correct way ////////////////////////////////////////////////////////////////////////////////////////////
function createNewUser() {
    let askName = prompt("What is ur name");
    let askLastName = prompt("What is your last name");
    let newUser = {
        firstName: askName,
        lastName: askLastName,
        login: function () {
            return askName[0].toLowerCase() + askLastName.toLowerCase()
        }
    };
    return newUser.login();
}

console.log(createNewUser());

///////////////////////////////////////////////////////////////////////////////////////////////////////////////